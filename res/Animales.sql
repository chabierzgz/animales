-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Fri Jun 21 12:37:02 2019 
-- * LUN file: D:\GAUDI\work\PRACTICAS\Animales.lun 
-- * Schema: Animales
-- ********************************************* 

-- Database Section
-- ________________ 

 drop database animalicos;
 create database animalicos;
 use animalicos;

-- Tables Section
-- _____________ 

create table animal (
     id_animal INT AUTO_INCREMENT,
     nb_alias varchar(200) not null,
     id_especie INT not null,
     id_raza INT,
     constraint ID_animal_ID primary key (id_animal));

create table especie (
     id_especie INT AUTO_INCREMENT,
     nb_especie varchar(200) not null,
     salvaje char not null,
     constraint ID_especie_ID primary key (id_especie));

create table raza (
     id_raza INT AUTO_INCREMENT,
     nb_raza varchar(200) not null,
     id_especie INT not null,
     constraint ID_raza_ID primary key (id_raza));

create table datos_animal (
     id_datos INT AUTO_INCREMENT,
     id_animal INT not null,
     imagen VARCHAR(1000),
     observaciones varchar(10000),
     constraint ID_datos_animal_ID primary key (id_datos),
     constraint SID_datos_anima_ID unique (id_animal));


-- Constraints Section
-- ___________________ 

alter table animal ADD CONSTRAINT fk_animal_especie
FOREIGN KEY (id_especie) REFERENCES especie(id_especie);

alter table animal ADD CONSTRAINT fk_animal_raza
FOREIGN KEY (id_raza) REFERENCES raza(id_raza);

alter table datos_animal ADD CONSTRAINT fk_datosAnimal_animal
FOREIGN KEY (id_animal) REFERENCES animal(id_animal);

alter table raza ADD CONSTRAINT fk_raza_especie
FOREIGN KEY (id_especie) REFERENCES especie(id_especie);


-- Index Section
-- _____________ 

create unique index ID_animal_IND
     on animal (id_animal);

create index REF_anima_espec_IND
     on animal (id_especie);

create unique index ID_especie_IND
     on especie (id_especie);

create unique index ID_raza_IND
     on raza (id_raza);

create index REF_raza_espec_IND
     on raza (id_especie);

create unique index ID_datos_animal_IND
     on datos_animal (id_datos);

create unique index SID_datos_anima_IND
     on datos_animal (id_animal);