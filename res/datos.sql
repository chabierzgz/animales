-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.3.16-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------


INSERT INTO `especie` (`id_especie`, `nb_especie`, `salvaje`) VALUES
	(1, 'Perro', '0'),
	(2, 'Gato', '0'),
	(3, 'Oso', '1');

INSERT INTO `raza` (`id_raza`, `nb_raza`, `id_especie`) VALUES
	(1, 'Podenco', 1),
	(2, 'Pastor Alemán', 1),
	(3, 'Siames', 2),
	(4, 'Polar', 3);

INSERT INTO `animal` (`id_animal`, `nb_alias`, `id_especie`, `id_raza`) VALUES
	(1, 'Mel', 1, 1),
	(2, 'Michifu', 2, 3),
	(3, 'Mijail', 3, 4),
	(4, 'Firulais', 1, NULL);

INSERT INTO `datos_animal` (`id_datos`, `id_animal`, `imagen`, `observaciones`) VALUES
	(1, 1, 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Podenco_chico_resized.jpg/245px-Podenco_chico_resized.jpg', 'Perra de Chabi'),
	(2, 2, 'https://www.affinity-petcare.com/advance/sites/default/files/img/articulos_gato/articulo25_main_cut%402x.png', NULL),
	(3, 3, 'https://t1.ea.ltmcdn.com/es/images/1/3/2/img_la_alimentacion_del_oso_polar_21231_600.jpg', ''),
	(4, 4, 'https://lanubedealgodon.com/wp-content/uploads/2016/12/perro-callejero-cambio-de-pantalla-2016-12-16-a-las-8.19.40.png', NULL);