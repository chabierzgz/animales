SELECT * FROM animal a 
INNER JOIN datos_animal da ON a.id_animal = da.id_animal
INNER JOIN especie e ON e.id_especie = a.id_especie
INNER JOIN raza r ON r.id_raza = a.id_raza
WHERE a.nb_alias LIKE 'Mel';