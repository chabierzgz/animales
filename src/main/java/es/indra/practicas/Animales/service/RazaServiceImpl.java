package es.indra.practicas.Animales.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.indra.practicas.Animales.entities.Raza;
import es.indra.practicas.Animales.repository.RazaRepository;


@Service
public class RazaServiceImpl implements RazaService {

  private RazaRepository razarepository;

  @Autowired
  public void setRazaRepository(final RazaRepository razarepository) {
    this.razarepository = razarepository;
  }

  @Override
  public Iterable<Raza> listAllRazasDeEspecie(final Integer id) {
    final Iterable<Raza> list = this.razarepository.listAllRazasDeEspecie(id);
    return list;
  }

  @Override
  public Iterable<Raza> listAllRazas() {
    return this.razarepository.findAll();
  }

  @Override
  public Raza getRazaById(final Integer id) {

    return this.razarepository.findById(id).orElse(null);
  }

  @Override
  public Raza saveRaza(final Raza raza) {
    return this.razarepository.save(raza);
  }

}
