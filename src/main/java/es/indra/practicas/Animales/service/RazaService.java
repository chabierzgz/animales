package es.indra.practicas.Animales.service;

import es.indra.practicas.Animales.entities.Raza;


public interface RazaService {

  Iterable<Raza> listAllRazas();

  Iterable<Raza> listAllRazasDeEspecie(Integer id);

  Raza getRazaById(Integer id);

  Raza saveRaza(Raza raza);

}
