package es.indra.practicas.Animales.service;

import es.indra.practicas.Animales.entities.Especie;

public interface EspecieService {
	Iterable<Especie> listAllEspecies();

	Especie getEspeciesById(Integer id);

	Especie saveEspecie(Especie especie);
}
