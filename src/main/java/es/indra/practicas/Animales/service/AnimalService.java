package es.indra.practicas.Animales.service;

import es.indra.practicas.Animales.entities.Animal;


/**
 * @author chabi
 */
public interface AnimalService {

  Iterable<Animal> listAllAnimals();

  Animal getAnimalById(Integer id);

  Animal saveAnimal(Animal animal);
}
