package es.indra.practicas.Animales.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.indra.practicas.Animales.entities.Especie;
import es.indra.practicas.Animales.repository.EspecieRepository;

@Service
public class EspecieServiceImpl implements EspecieService {

	private EspecieRepository especieRepository;

	@Override
	public Iterable<Especie> listAllEspecies() {

		return this.especieRepository.findAll();
	}

	@Override
	public Especie getEspeciesById(final Integer id) {

		return this.especieRepository.findById(id).orElse(null);

	}

	@Override
	public Especie saveEspecie(Especie especie) {

		return this.especieRepository.save(especie);
	}

	@Autowired
	public void setEspecieRepository(final EspecieRepository especieRepository) {
		this.especieRepository = especieRepository;
	}
}
