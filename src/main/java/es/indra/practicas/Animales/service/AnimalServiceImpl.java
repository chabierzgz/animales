package es.indra.practicas.Animales.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.indra.practicas.Animales.entities.Animal;
import es.indra.practicas.Animales.repository.AnimalRepository;

/**
 * @author chabi
 */
@Service
public class AnimalServiceImpl implements AnimalService {

	// @Autowired
	private AnimalRepository animalRepository;

	@Autowired
	public void setAnimalRepository(final AnimalRepository productRepository) {
		this.animalRepository = productRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.indra.practicas.Animales.service.AnimalService#listAllAnimals()
	 */
	@Override
	public Iterable<Animal> listAllAnimals() {
		return this.animalRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.indra.practicas.Animales.service.AnimalService#getAnimalById(java.lang.
	 * Integer)
	 */
	@Override
	public Animal getAnimalById(final Integer id) {
		return this.animalRepository.findById(id).orElse(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.indra.practicas.Animales.service.AnimalService#saveProduct(es.indra.
	 * practicas.Animales.entities1.Animal)
	 */
	@Override
	public Animal saveAnimal(final Animal animal) {
		return this.animalRepository.save(animal);
	}

}
