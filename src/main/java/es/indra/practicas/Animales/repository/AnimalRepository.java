package es.indra.practicas.Animales.repository;

import org.springframework.data.repository.CrudRepository;

import es.indra.practicas.Animales.entities.Animal;


/**
 * @author chabi
 */
public interface AnimalRepository extends CrudRepository<Animal, Integer> {

}
