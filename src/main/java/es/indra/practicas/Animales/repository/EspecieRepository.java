package es.indra.practicas.Animales.repository;

import org.springframework.data.repository.CrudRepository;

import es.indra.practicas.Animales.entities.Especie;

public interface EspecieRepository extends CrudRepository<Especie, Integer> {

}
