package es.indra.practicas.Animales.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import es.indra.practicas.Animales.entities.Raza;


public interface RazaRepository extends CrudRepository<Raza, Integer> {

  @Query("from Raza r join r.especie e where e.idEspecie=:idEspecie")
  public Iterable<Raza> listAllRazasDeEspecie(@Param("idEspecie") Integer idEspecie);

}
