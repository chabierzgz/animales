package es.indra.practicas.Animales.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import lombok.Data;


/**
 * @author chabi
 */
@Entity
@Table(name="raza")
@Data
//@NamedQuery(name = "Raza.listAllRazasDeEspecie",
//query = "SELECT r FROM Raza r where r.especie.idEspecie = ? ORDER BY r.nombreRaza DESC")
public class Raza  implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_raza")
  private Integer idRaza;
  
  @Column(name = "nb_raza")
  private String nombreRaza;
  
  @OneToOne
  @JoinColumn(name = "id_especie")
  private Especie especie = new Especie();
  
  
}
