package es.indra.practicas.Animales.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Data;


/**
 * @author chabi
 */
@Entity
@Table(name="animal")
@Data
public class Animal  implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer idAnimal;

  @Column(name = "nb_alias")
  private String nbAlias;
  
  
  //relaciones

  @OneToOne(mappedBy = "animal", fetch = FetchType.LAZY)
  @JoinColumn(name = "id_datos")
  private final DatosAnimal datosAnimal = new DatosAnimal();

  @OneToOne
  @JoinColumn(name = "id_especie")
  private final Especie especie = new Especie();
  
  @OneToOne
  @NotFound(action=NotFoundAction.IGNORE)
  @JoinColumn(name = "id_raza")
  private final Raza raza = new Raza();

}
