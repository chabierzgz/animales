package es.indra.practicas.Animales.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;


/**
 * @author chabi
 */
@Entity
@Table(name="datos_animal")
@Data
public class DatosAnimal  implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_datos")
  private Integer idDatos;

  @Column(name = "imagen")
  private String imagen;

  @Column(name = "observaciones")
  private String observaciones;
  
  @OneToOne
  @JoinColumn(name = "id_animal")
  private Animal animal;

}
