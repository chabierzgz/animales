package es.indra.practicas.Animales.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Data;


/**
 * @author chabi
 */
@Entity
@Table(name="especie")
@Data
public class Especie  implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_especie")
  private Integer idEspecie;
  
  @Column(name = "nb_especie")
  private String nombreEspecie;
  
  @Column(name = "salvaje")
  private Boolean salvaje;
  
  @OneToMany(mappedBy = "especie")
  private List<Raza> raza = new ArrayList();
  
}
