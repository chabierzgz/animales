package es.indra.practicas.Animales.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import es.indra.practicas.Animales.entities.Animal;
import es.indra.practicas.Animales.service.AnimalService;

@Controller
public class ListadoController {
	@Autowired
	private AnimalService animalService;

	@RequestMapping("/listado")
	public String index(final Model model) {
		final Iterable<Animal> animales = this.animalService.listAllAnimals();
		model.addAttribute("animales", animales);
		return "index";
	}
}
