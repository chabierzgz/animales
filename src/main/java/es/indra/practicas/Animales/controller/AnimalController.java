package es.indra.practicas.Animales.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import es.indra.practicas.Animales.entities.Animal;
import es.indra.practicas.Animales.service.AnimalService;
import es.indra.practicas.Animales.service.EspecieService;
import es.indra.practicas.Animales.service.RazaService;


@Controller
@RequestMapping("/animal")
public class AnimalController {

  @Autowired
  private AnimalService animalService;

  @Autowired
  private EspecieService especieService;

  @Autowired
  private RazaService razaService;

  @RequestMapping("/nuevo")
  public String addNewRegistro(final Model model) {
    model.addAttribute("animal", new Animal());
    model.addAttribute("especies", this.especieService.listAllEspecies());
    model.addAttribute("razas", this.razaService.listAllRazas());
    model.addAttribute("guardado", 0);
    return "nuevoRegistro";
  }

  @PostMapping("/save")
  public String saveAnimal(final Model model, Animal animal) {
    animal = this.animalService.saveAnimal(animal);

    model.addAttribute("especies", this.especieService.listAllEspecies());
    model.addAttribute("razas", this.razaService.listAllRazas());

    if (animal != null) {
      model.addAttribute("guardado", 1);
      model.addAttribute("animal", new Animal());
    } else {
      model.addAttribute("guardado", 2);
      model.addAttribute("animal", animal);
    }

    return "nuevoRegistro";
  }

  @RequestMapping("/listado")
  public String index(final Model model) {
    final Iterable<Animal> animales = this.animalService.listAllAnimals();
    model.addAttribute("animales", animales);
    return "index";

  }
}
