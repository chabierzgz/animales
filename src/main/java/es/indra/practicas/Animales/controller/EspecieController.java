package es.indra.practicas.Animales.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import es.indra.practicas.Animales.entities.Especie;
import es.indra.practicas.Animales.service.EspecieService;


@Controller
@RequestMapping("/especie")
public class EspecieController {

  @Autowired
  private EspecieService especieService;

  @RequestMapping("/listado")
  public String index(final Model model) {
    final Iterable<Especie> especies = this.especieService.listAllEspecies();
    model.addAttribute("especies", especies);
    return "listaespecie";
  }

  @RequestMapping("/nueva")
  public String addNewEspecie(final Model model) {
    model.addAttribute("especie", new Especie());
    model.addAttribute("guardado", 0);
    return "nuevaEspecie";
  }

  @PostMapping("/save")
  public String saveEspecie(final Model model, final Especie especie) {
    this.especieService.saveEspecie(especie);
    if (especie != null) {
      model.addAttribute("guardado", 1);
      model.addAttribute("especie", new Especie());
    } else {
      model.addAttribute("guardado", 2);
      model.addAttribute("especie", especie);
    }
    return "nuevaEspecie";
  }
}
