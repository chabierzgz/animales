package es.indra.practicas.Animales.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.indra.practicas.Animales.entities.Raza;
import es.indra.practicas.Animales.service.EspecieService;
import es.indra.practicas.Animales.service.RazaService;


@Controller
@RequestMapping("/raza")
public class RazaController {
  @Autowired
  private RazaService razaService;

  @Autowired
  private EspecieService especieService;

  @PostMapping("/lista")
  public String lista(final Model model) {
    model.addAttribute("razas", this.razaService.listAllRazas());
    return "listaraza";
  }

  @RequestMapping("/nueva")
  public String addNewRaza(final Model model) {
    model.addAttribute("especies", this.especieService.listAllEspecies());
    model.addAttribute("raza", new Raza());
    model.addAttribute("guardado", 0);
    return "nuevaRaza";
  }

  @PostMapping("/save")
  public String saveRaza(final Model model, Raza raza) {
    raza = this.razaService.saveRaza(raza);
    if (raza != null) {
      model.addAttribute("guardado", 1);
      model.addAttribute("raza", new Raza());
    } else {
      model.addAttribute("guardado", 2);
      model.addAttribute("raza", raza);
    }
    model.addAttribute("especies", this.especieService.listAllEspecies());
    return "nuevaRaza";
  }

 

  @GetMapping(path = "/deEspecie/{id}", produces = "application/json")
  @ResponseBody
  public Iterable<Raza> getRazasDeEspecie(@PathVariable final Integer id) {
    return this.razaService.listAllRazasDeEspecie(id);
  }

}
