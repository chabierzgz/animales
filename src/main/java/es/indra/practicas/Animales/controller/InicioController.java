package es.indra.practicas.Animales.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import es.indra.practicas.Animales.entities.Animal;
import es.indra.practicas.Animales.service.AnimalService;


/**
 * @author chabi
 */
@Controller
@RequestMapping("/")
public class InicioController {

  @Autowired
  private AnimalService animalService;

  @Autowired
  private MessageSource mensajes;

  @RequestMapping("/index")
  public String maqueteacion(final Model model) {
    final Iterable<Animal> animales = this.animalService.listAllAnimals();
    final int numAnimales = ((Collection<?>) animales).size();
    final Animal animal = this.animalService.getAnimalById(new Double((Math.random() * numAnimales) + 1).intValue());
    model.addAttribute("animal", animal);
    return "maquetacion";
  }

  @RequestMapping("/")
  public String index(final Model model) {
    final Iterable<Animal> animales = this.animalService.listAllAnimals();
    final int numAnimales = ((Collection<?>) animales).size();
    final Animal animal = this.animalService.getAnimalById(new Double((Math.random() * numAnimales) + 1).intValue());
    model.addAttribute("animal", animal);
    return "maquetacion";
  }

  public String getSaludo(final String nombre) {
    return this.mensajes.getMessage("saludo.usuario", new String[] { nombre }, LocaleContextHolder.getLocale());
  }

}
