package es.indra.practicas.Animales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import es.indra.practicas.Animales.entities.Animal;


@SpringBootApplication
// @EnableJpaRepositories(basePackageClasses = { AnimalRepository.class })
public class AnimalesApplication {

  public static void main(final String[] args) {
    SpringApplication.run(AnimalesApplication.class, args);
  }

}
